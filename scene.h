#ifndef GT_SCENE_H
#define GT_SCENE_H

#include <stdio.h>

#include <game/scene/import.h>
#include <game/scene/scene.h>
#include <game/render/lights_buffer_object.h>
#include <game/render/camera_buffer_object.h>

struct GameTest {
    struct Node* activeCamera;
    unsigned nbCameras, nbLights, activeCamIdx;
    struct Node** cameras;
    struct Node** lights;
    struct Node* activePointLights[MAX_POINT_LIGHTS];
    struct Node* activeDirectionalLights[MAX_DIRECTIONAL_LIGHTS];

    struct Scene scene;
    struct ImportMetadata importData;
    struct SharedData shared;
};

void set_active_camera(struct GameTest* data, unsigned camIdx);
void next_active_camera(struct GameTest* data);
int set_light_status(struct GameTest* data, struct Node* light, int on);

int game_test_init(struct GameTest* data, FILE* ogexFile);

void game_test_render(struct GameTest* gameTest);

void game_test_free(struct GameTest* gameTest);

#endif
