#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <game/math/linear_algebra.h>
#include <game/render/camera.h>
#include <game/render/viewer.h>
#include <game/render/camera_buffer_object.h>

#include "scene.h"
#include "callbacks.h"

int running;

void resize_callback(struct Viewer* viewer, void* data) {
    struct GameTest* gameTest = data;

    if (gameTest->activeCamera) {
        camera_set_ratio(((float)viewer->width) / ((float)viewer->height), gameTest->activeCamera->data.camera->projection);
        camera_buffer_object_update_projection(&gameTest->scene.camera,
                                               MAT_CONST_CAST(gameTest->activeCamera->data.camera->projection));
    }
}

void cursor_rotate_object(struct Viewer* viewer, double xpos, double ypos, double dx, double dy,
                          int buttonLeft, int buttonMiddle, int buttonRight, void* data) {
	Vec3 x = {0, 1, 0}, y = {1, 0, 0};

	if (buttonLeft) {
		node_rotate(data, x, 4.0 * dx / viewer->width);
		node_rotate(data, y, 4.0 * dy / viewer->height);
	}
}

void cursor_rotate_camera(struct Viewer* viewer, double xpos, double ypos, double dx, double dy,
                          int buttonLeft, int buttonMiddle, int buttonRight, void* data) {
	Vec3 axisY = {0, 1, 0};
	Vec3 axisX = {1, 0, 0};
    struct GameTest* gameTest = data;

	if (buttonLeft) {
        if (gameTest->activeCamera) {
            node_rotate(gameTest->activeCamera, axisY, dx / viewer->width);
            node_slew(gameTest->activeCamera, axisX, dy / viewer->height);
        }
    }
}

void key_callback(struct Viewer* viewer, int key, int scancode, int action, int mods, void* userData) {
	Vec3 axis = {0, 1, 0};
    Vec3 up = {0, 0.1, 0};
    Vec3 down = {0, -0.1, 0};
    Vec3 right = {0.1, 0, 0};
    Vec3 left = {-0.1, 0, 0};
    Vec3 forward = {0, 0, -0.1};
    Vec3 backward = {0, 0, 0.1};
    struct GameTest* gameTest = userData;

	switch (key) {
		case GLFW_KEY_ESCAPE:
			running = 0;
			break;
		case GLFW_KEY_PAGE_UP:
		case GLFW_KEY_Q:
            /* TODO: cam up */
			break;
		case GLFW_KEY_PAGE_DOWN:
		case GLFW_KEY_E:
            /* TODO: cam down */
			break;
		case GLFW_KEY_LEFT:
		case GLFW_KEY_A:
            if (gameTest->activeCamera)
                node_shift(gameTest->activeCamera, left);
			break;
		case GLFW_KEY_RIGHT:
		case GLFW_KEY_D:
            if (gameTest->activeCamera)
                node_shift(gameTest->activeCamera, right);
			break;
		case GLFW_KEY_DOWN:
		case GLFW_KEY_S:
            if (gameTest->activeCamera)
                node_shift(gameTest->activeCamera, backward);
			break;
		case GLFW_KEY_UP:
		case GLFW_KEY_W:
            if (gameTest->activeCamera)
                node_shift(gameTest->activeCamera, forward);
			break;
        case GLFW_KEY_F1:
            if (gameTest->nbCameras && action) {
                next_active_camera(gameTest);
            }
		case GLFW_KEY_F12:
			viewer_screenshot(viewer, "screenshot.png");
			break;
	}
}

void close_callback(struct Viewer* viewer, void* userData) {
	running = 0;
}
