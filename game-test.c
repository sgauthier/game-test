#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <game/init.h>
#include <game/skybox.h>
#include <game/render/viewer.h>
#include <game/scene/opengex.h>

#include "callbacks.h"
#include "scene.h"

#define LIGHT_INTENSITY 0.4

static void usage(const char* progname) {
    printf("Usage: %s <ogex file>\n", progname);
}

int main(int argc, char** argv) {
    char title[1024];
    struct Viewer* viewer = NULL;
    double dt, t = 0, ct = 0, cf = 0, fps = 0;
    int ret = 0;
    FILE* ogexFile;
    struct GameTest gameTest;

    if (!game_init("../game/shaders")) {
        return 1;
    }

    if (argc != 2) {
        usage(argv[0]);
        return 1;
    }

    if (!(ogexFile = fopen(argv[1], "r"))) {
        fprintf(stderr, "Error: cannot open file : %s\n", argv[1]);
        return 1;
    }

    if (!(viewer = viewer_new(640, 480, "Game test"))) {
        fprintf(stderr, "Error: cannot start viewer\n");
        ret = 1;
    } else {
        if (!game_test_init(&gameTest, ogexFile))
            return 1;

        viewer->key_callback = key_callback;
        viewer->cursor_callback = cursor_rotate_camera;
        viewer->close_callback = close_callback;
        viewer->resize_callback = resize_callback;
        viewer->callbackData = &gameTest;
        glfwSwapInterval(1);
        running = 1;
        for (t = 0; running ; t += dt) {
            dt = viewer_next_frame(viewer);
            viewer_process_events(viewer);
            game_test_render(&gameTest);
            ct += dt;
            cf++;
            if (ct > 1.0) {
                fps = cf / ct;
                ct = cf = 0;
                sprintf(title, "%s - %u FPS - %d rendered obj", "Game test", (unsigned int)fps, gameTest.scene.nRender);
                viewer_set_title(viewer, title);
            }
        }
    }

    fclose(ogexFile);
    game_test_free(&gameTest);
    viewer_free(viewer);
    game_free();
    return ret;
}
