#include <stdlib.h>

#include <game/scene/opengex.h>

#include "scene.h"

void set_active_camera(struct GameTest* data, unsigned camIdx) {
    data->activeCamera = data->cameras[camIdx];
    data->activeCamIdx = camIdx;
    camera_buffer_object_update_view_and_position(&data->scene.camera,
                        MAT_CONST_CAST(data->activeCamera->data.camera->view));
}

void next_active_camera(struct GameTest* data) {
    data->activeCamIdx = (data->activeCamIdx+1)%data->nbCameras;
    set_active_camera(data, data->activeCamIdx);
    printf("Switching to cam %d\n", data->activeCamIdx);
}

static void init_lights(struct GameTest* gameTest) {
    unsigned i;
    unsigned plUbo = 0, dlUbo = 0;
    struct Scene* scene = &gameTest->scene;

    if (gameTest->importData.nbLightNodes) {
        printf("Found %d light(s) in scene\n", gameTest->importData.nbLightNodes);
        gameTest->nbLights = gameTest->importData.nbLightNodes;
        gameTest->lights = gameTest->importData.lightNodes;
        for (i = 0; i < gameTest->nbLights; i++) {
            struct Node* node = gameTest->lights[i];
            switch (node->type) {
                case NODE_PLIGHT:
                    if (plUbo < MAX_POINT_LIGHTS) {
                        gameTest->activePointLights[plUbo] = node;
                        lights_buffer_object_update_plight(&scene->lights, node->data.plight, plUbo);
                        plUbo++;
                    } else {
                        fprintf(stderr, "Warning: MAX_POINT_LIGHTS reached, skipping light\n");
                    }
                    break;
                case NODE_DLIGHT:
                    if (dlUbo < MAX_DIRECTIONAL_LIGHTS) {
                        gameTest->activeDirectionalLights[dlUbo] = node;
                        lights_buffer_object_update_dlight(&scene->lights, node->data.dlight, dlUbo);
                        dlUbo++;
                    } else {
                        fprintf(stderr, "Warning: MAX_DIRECTIONAL_LIGHTS reached, skipping light\n");
                    }
                    break;
                default:
                    printf("Skipping light\n");
                    break;
            }
        }
        printf("%d %d\n", plUbo, dlUbo);
        lights_buffer_object_update_nplight(&scene->lights, plUbo);
        lights_buffer_object_update_ndlight(&scene->lights, dlUbo);
    }
}

static void init_cams(struct GameTest* gameTest) {
    gameTest->nbCameras = 0;
    gameTest->cameras = NULL;
    gameTest->activeCamera = NULL;
    if (gameTest->importData.nbCameraNodes) {
        gameTest->nbCameras = gameTest->importData.nbCameraNodes;
        gameTest->cameras = gameTest->importData.cameraNodes;
        printf("Found %d camera(s) in scene, setting 0 as active\n", gameTest->importData.nbCameraNodes);
    } else {
        struct Node* camNode;
        struct Camera* cam;
        printf("Warning: no cam found in scene, initializing fallback cam\n");
        if (!(camNode = malloc(sizeof(*camNode))))
            return;
        if (!(cam = malloc(sizeof(*cam)))) {
            free(camNode);
            return;
        }
        if (!(gameTest->cameras = malloc(sizeof(*(gameTest->cameras))))) {
            free(camNode);
            free(cam);
            return;
        }
        node_init(camNode);
        node_set_camera(camNode, cam);
        scene_add(&gameTest->scene, camNode);
        gameTest->nbCameras = 1;
        gameTest->cameras[0] = camNode;
        gameTest->activeCamera = camNode;
        gameTest->activeCamIdx = 0;
    }
    set_active_camera(gameTest, 0);
}

static void free_node(struct Node* node) {
    switch (node->type) {
        case NODE_GEOMETRY:
            free(node->data.geometry);
            break;
        default:
            break;
    }
    if (node->father)
        free(node);
}

int game_test_init(struct GameTest* gameTest, FILE* ogexFile) {
    unsigned i;

    gameTest->activeCamera = NULL;
    gameTest->nbCameras = 0;
    gameTest->activeCamIdx = 0;
    gameTest->nbLights = 0;

    gameTest->cameras = NULL;
    gameTest->lights = NULL;
    for (i = 0; i < MAX_POINT_LIGHTS; i++) gameTest->activePointLights[i] = NULL;
    for (i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) gameTest->activeDirectionalLights[i] = NULL;

    if (!scene_init(&gameTest->scene, NULL)) {
        fprintf(stderr, "Error: scene_init failed\n");
        return 0;
    }
    if (!ogex_load(&gameTest->scene, ogexFile, &gameTest->shared, &gameTest->importData)) {
        fprintf(stderr, "Error: ogex_load failed\n");
        scene_free(&gameTest->scene, free_node);
        return 0;
    }
    init_cams(gameTest);
    init_lights(gameTest);
    return 1;
}

void game_test_free(struct GameTest* gameTest) {
    import_free_shared_data(&gameTest->shared);
    import_free_metadata(&gameTest->importData);
    scene_free(&gameTest->scene, free_node);
}

static void changed_callback(struct Scene* scene, struct Node* node, void* data) {
    struct GameTest* gameTest = data;
    unsigned i;

    switch (node->type) {
        case NODE_DLIGHT:
            for (i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) {
                if (node == gameTest->activeDirectionalLights[i]) {
                    lights_buffer_object_update_dlight(&scene->lights, node->data.dlight, i);
                    break;
                }
            }
            break;
        case NODE_PLIGHT:
            for (i = 0; i < MAX_POINT_LIGHTS; i++) {
                if (node == gameTest->activePointLights[i]) {
                    lights_buffer_object_update_plight(&scene->lights, node->data.plight, i);
                    break;
                }
            }
            break;
        case NODE_CAMERA:
            if (node == gameTest->activeCamera) {
                camera_buffer_object_update_view(&scene->camera, MAT_CONST_CAST(node->data.camera->view));
                camera_buffer_object_update_position(&scene->camera, node->position);
            }
            break;
        default:;
    }
}

void game_test_render(struct GameTest* gameTest) {
    uniform_buffer_send(&gameTest->scene.camera);
    uniform_buffer_send(&gameTest->scene.lights);
    scene_update_nodes(&gameTest->scene, changed_callback, gameTest);
    scene_update_render_queue(&gameTest->scene, MAT_CONST_CAST(gameTest->activeCamera->data.camera->view),
                              MAT_CONST_CAST(gameTest->activeCamera->data.camera->projection));
    scene_render(&gameTest->scene);
}
