DEPS := game

CFLAGS ?= -std=c89 -pedantic -march=native -Wall -g
CFLAGS += $(shell pkg-config --cflags $(DEPS)) -I.
LDFLAGS += $(shell pkg-config --libs $(DEPS)) -lm

OBJECTS := $(patsubst %.c,%.o,$(wildcard *.c))

.PHONY: all
all: game-test

game-test: $(OBJECTS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(OBJECTS) game-test
